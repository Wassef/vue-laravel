import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

import * as types from './mutation-types'



Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';


// initial state
const state = {
  added: [],
  all:[],
}

// getters
const getters = {
	allProducts: state => state.all, // would need action/mutation if data fetched async
	getNumberOfProducts: state => (state.all) ? state.all.length : 0,
	cartProducts: state => {
		return state.added.map(({ id, quantity }) => {
			const product = state.all.find(p => p.id === id)
			return {
				name: product.name,
				price: product.price,
				image: product.image,
				quantity
			}
		})
	}
}

// actions
const actions = {

	addToCart({ commit }, product){
		commit(types.ADD_TO_CART, {
			id: product.id
		})
	},
	fetchProducts(store){
		axios.get('http://aqarat.aklbaety.com/api/v1/products')
			.then(response => {
				// JSON responses are automatically parsed.
				console.log( response.data.data.products);
				store.commit('setProducts', response.data.data.products)
				// state.all=response.data.data.products;
			})

	}
}

// mutations
const mutations = {
	setProducts (state, something) {
		this.state.all = something
	},
	[types.ADD_TO_CART] (state, { id }) {
	    const record = state.added.find(p => p.id === id)

	    if (!record) {
	      state.added.push({
	        id,
	        quantity: 1
	      })
	    } else {
	      record.quantity++
	    }
	  }
}

// one store for entire application
export default new Vuex.Store({
	state,
	strict: debug,
	getters,
	actions,
	mutations
})
