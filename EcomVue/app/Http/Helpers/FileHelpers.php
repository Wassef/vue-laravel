<?php



/**
 * Setting Name
 * @param $name
 * @return mixed
 */
function getsetting($name)
{
    $setting=\App\Setting::where('name',$name)->first();
    if (!$setting) return "";
    return $setting->value();
}

function popup($name, $validator = null,$custom = null)
{
    $ms = 10000;
    if ($validator != null) {

        return alert()->error($validator->errors()->first())->autoclose($ms);

    }

    if ($name == 'add') {
        return alert()->success('تم الاضافه بنجاح')->autoclose($ms);
    } elseif ($name == 'update') {
        return alert()->success('تم التعديل بنجاح')->autoclose($ms);
    }elseif ($name == 'delete'){
        return alert()->success('تم الحذف بنجاح')->autoclose($ms);
    }elseif ($name == 'custom'){
        return alert()->success($custom)->autoclose($ms);
    }

}

/**
 * Upload Path
 * @return string
 */
function uploadpath()
{
    return 'photos';
}

/**
 * Get Image
 * @param $filename
 * @return string
 */
function getimg($filename)
{
    $base_url = url('/');
    return $base_url.'/storage/'.$filename;
}

/**
 * Upload an image
 * @param $img
 */
function uploader($request,$img_name)
{
    $path = \Storage::disk('public')->putFile(uploadpath(), $request->file($img_name));
    return $path;
}

function statuss($status = null)
{
    $collect = [
        'new'=>'جديد',
        'prepared'=>'قيد التجهيز',
        'delivered'=>'تم التوصيل',
        'canceled'=>'تم الالغاء'
    ];
    if($status == null)
        return $collect;

    return $collect[$status];
}


function multiUploader($request,$img_name,$model,$onId=null){
    foreach ($request[$img_name] as $image){
        $filename = rand(99999, 99999999) . $image->getClientOriginalName();
        $path = \Storage::disk('public')->putFile(uploadpath(), $image);
        $model->create(['image'=>$path]+$onId);
    }
    return true;
}

function deleteImg($img_name)
{
    \Storage::disk('public')->delete(uploadpath(),$img_name);
    return True;
}




function status()
{
    $array = [
        '1'=>'مفعل',
        '0'=>'غير مفعل',
    ];
    return $array;
}




function roles()
{
    $array = [
        'client'=>'عميل',
        'owner'=>'صاحب منشأة',
    ];
    return $array;
}


function cities()
{
    $cities = App\City::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $cities;
}


function trades()
{
    $cities = App\Trade::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $cities;
}


function products()
{
    $cities = App\Product::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $cities;
}

function sub_categories()
{
    $cities = App\SubCategory::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $cities;
}



function user_firms()
{
    $user_firms = App\Firm::whereUserId(user()->id)->get()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $user_firms;
}



function countries()
{
    $countries = App\Country::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $countries;
}

function regions()
{
    $countries = App\Region::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $countries;
}

function users()
{
    $countries = App\User::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['name']];
    });
    return $countries;
}
function permissions()
{
    $countries = App\Permission::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['title']];
    });
    return $countries;
}


function categories()
{
    $countries = App\Category::all()->mapWithKeys(function ($item) {
        return [$item['id'] => $item['ar_name']];
    });
    return $countries;
}




function GenerateCode() {
    $code = str_random(6); // better than rand()
    // call the same function if the barcode exists already
    if (UniqueCode($code)) {
        return GenerateCode();
    }
    // otherwise, it's valid and can be used
    return $code;
}

function UniqueCode($code)
{
    return \App\Coupon::where('code',$code)->first();
}

function fcm_server_key(){
    return 'AAAAdTgp7Lk:APA91bEdECFg296xuJhdtocpK6SIENoV8h3_vMF7zQSGwNeBv2bMhXOzOlMA_yXx6Z2Xv7ECEWnMZZYSK5xwoab0N77FkCs90st20QxR8gWKBsTbJbviu29YguAEiOzqnEQhTYBiDGuZ';
}

function user(){
    return auth()->user();
}

function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
    // Calculate the distance in degrees
    $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

    // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
    switch($unit) {
        case 'km':
            $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
            break;
        case 'mi':
            $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
            break;
        case 'nmi':
            $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
    }
    return round($distance, $decimals);
}


function check_null($var)
{
    if(isset($var))
    {
        if(!is_null($var))return $var;
        if(is_int($var)) return 0 ;
        return "";
    }
    return "";

}


function except_values($collection,$arr)
{
    $collection = $collection->map(function ($item) use($arr) {
        return collect($item)->except($arr)->toArray();
    });
    return $collection;
}

function slug($name)
{
    return $name.'-'.str_random(30);
}

function GetChargeFees()
{
    return 0;
}


function getDayPrice($date,$place_id)
{
    $today =$date;
    $current_day = $date;
    $place = \App\Place::find($place_id);
    $special_days_ids = $place->days->pluck('id');
    $special_day = \App\PlaceDayPrice::whereIn('id',$special_days_ids)->where('day',$today)->first();
    if($special_day)return $special_day->price;
    if ($current_day == "Friday" || $current_day == "Wednesday") return $place->weekend_price;
    return $place->mid_week_price;
}


function returnDates($fromdate, $todate) {
    $fromdate = new \DateTime($fromdate);
    $todate = new \DateTime($todate);
    return new \DatePeriod(
        $fromdate,
        new \DateInterval('P1D'),
        $todate->modify('+1 day')
    );
}

function notifyByFirebase($title, $body, $tokens, $data = [], $type='client' , $is_notification = true)
{
// https://gist.github.com/rolinger/d6500d65128db95f004041c2b636753a
// API access key from Google FCM App Console
    // env('FCM_API_ACCESS_KEY'));

//    $singleID = 'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd';
//    $registrationIDs = array(
//        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd',
//        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd',
//        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd'
//    );
    $registrationIDs = $tokens;

// prep the bundle
// to see all the options for FCM to/notification payload:
// https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support

// 'vibrate' available in GCM, but not in FCM
    $fcmMsg = array(
        'body' => $body,
        'title' => $title,
        'sound' => "default",
        'color' => "#203E78"
    );
// I haven't figured 'color' out yet.
// On one phone 'color' was the background color behind the actual app icon.  (ie Samsung Galaxy S5)
// On another phone, it was the color of the app icon. (ie: LG K20 Plush)

// 'to' => $singleID ;      // expecting a single ID
// 'registration_ids' => $registrationIDs ;     // expects an array of ids
// 'priority' => 'high' ; // options are normal and high, if not set, defaults to high.
    $fcmFields = array(
        'registration_ids' => $registrationIDs,
        'priority' => 'high',
    );
    if ($is_notification)
    {
        $fcmFields['notification'] = $fcmMsg;
    }

    $headers = array(
        'Authorization: key=AAAAuNox9r4:APA91bHS38_vfFaoawAREtKfTDgQIvOROzjr2fmBNuuDJC3CDHQX-QZWfWegawI3rnoPh4fQVhm6kakfkMAfZ1tN3-umZ6_VWhoPv4BpXpotg-HRXRpfg0kfv5auZiC1NZ3jC2zGMFLb' ,
        'Content-Type: application/json'
    );

    info("API_ACCESS_KEY_client: ".env('API_ACCESS_KEY_client'));
    info("PUSHER_APP_ID: ".env('PUSHER_APP_ID'));

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
