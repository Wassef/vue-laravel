<?php

namespace App\Http\Controllers\Admin;

use App\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index')->with('items',Product::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.products.add');
    }

    public function show ($id){

       return "عرض";

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'name'=>'required|string|',
            'price'=>'required|string|numeric',
            'image'=>'required|image|',
            'quantity' => 'required|string|',
        ]);
        $image = uploader($request,'image');
        $inputs=$request->all();
        $inputs['image']=$image;
        $inputs['user_id']=auth()->id();
        Product::create($inputs);
        alert()->success('تم اضافة المنتج بنجاح !')->autoclose(5000);
        return back();
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $product=Product::find($id);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
