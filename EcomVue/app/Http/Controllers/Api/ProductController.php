<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Http\Traits\ApiResponses;
use App\Http\Controllers\Controller;



class ProductController extends Controller
{
    use ApiResponses;

    /**
     *
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate($this->paginateNumber);
        $products = new \App\Http\Resources\Product($products);
        return $this->apiResponse($products);
    }

}
