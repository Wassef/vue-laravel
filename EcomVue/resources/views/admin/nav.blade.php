<!-- Main sidebar -->
<div class="sidebar sidebar-main ">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left">
                        <img src="" class="img-circle img-sm" alt="">
                    </a>

                    <div class="media-body">
                        <span class="media-heading text-semibold">{{Auth::user()->name}}</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i> مدير الموقع
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">


                <ul class="navigation navigation-main navigation-accordion">


                    <!-- Main -->
                    <li class="navigation-header"><span>الاعدادات الرئيسية</span> <i class="icon-menu"
                    title="Main pages"></i></li>





                    <li class="navigation-header"><span>المستخدمين  </span> <i class="icon-menu" title="Main pages"></i></li>

                    <li>
                        <a href="#"><i class="icon-megaphone"></i> <span>المستخدمين</span></a>
                        <ul>
                            <li class="{{ (Request::is('dashboard/admin') ? 'active' : '')}}"><a
                                        href="{{route('admin.admin.index')}}"><i class="icon-list"></i> كل  المستخدمين </a>
                            </li>
                            <li class="{{  (Request::is('dashboard/admin/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.admin.create')}}"><i class="icon-megaphone"></i> اضافة مستخدم جديد</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-megaphone"></i> <span>المنتجات</span></a>
                        <ul>
                            <li class="{{ (Request::is('dashboard/product') ? 'active' : '')}}"><a
                                        href="{{route('admin.product.index')}}"><i class="icon-list"></i> كل  المنتجات </a>
                            </li>
                            <li class="{{  (Request::is('dashboard/product/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.product.create')}}"><i class="icon-megaphone"></i> اضافة منتج جديد</a></li>
                        </ul>
                    </li>


         

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
