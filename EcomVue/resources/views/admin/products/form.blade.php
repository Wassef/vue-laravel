
@if (count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif


<div class="form-group col-md-12 pull-left">
    <label>الاسم  </label>  <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
</div>


<div class="form-group col-md-6 pull-left">
    <label> السعر </label>   <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("price",null,['class'=>'form-control','placeholder'=>'السعر'])!!}
</div>

<div class="form-group col-md-6 pull-left">
    <label> الكمية </label>   <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("quantity",null,['class'=>'form-control','placeholder'=>'الكمية'])!!}
</div>

<div class="form-group col-md-6 pull-left">
    <label>صورة </label>   <span class="label bg-danger help-inline">مطلوب</span>
    <input type="file" class="form-control" name="image">
</div>






<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

@push('scripts')

@endpush