@extends('admin.layout')
@section('header')
    @endsection
@section('title')
الصفحة الرئيسية والاحصائيات
@endsection

@section('content')


    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-grey-400 has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin"> </h3>
                        <span class="text-uppercase text-size-mini">عدد عضويات الاداره</span>
                        <br>
                        {{$admins }}
                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-users  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-success has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin">  </h3>
                        <span class="text-uppercase text-size-mini">عدد عضويات العملاء</span>
                        <br>
                        {{$clients}}
                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-users2  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-info has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin">  </h3>
                        <span class="text-uppercase text-size-mini">عدد عضويات مزودى الخدمه</span>
                        <br>
                        {{$providers}}
                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-user-tie  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-pink-400 has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin">  </h3>
                        <span class="text-uppercase text-size-mini">عدد الدول </span>
                        <br>
                        {{$countries}}
                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-location4  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-teal-700 has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin">  </h3>
                        <span class="text-uppercase text-size-mini">عدد المدن </span>
                        <br>
                        {{$cities}}
                    </div>
                    <div class="media-right media-middle">
                        <i class="icon-location3  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-blue-700 has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin"> </h3>
                        <span class="text-uppercase text-size-mini">عدد الأحياء </span>
                        <br>
                        {{$regions}}
                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-location3  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-grey has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin">  </h3>
                        <span class="text-uppercase text-size-mini">عدد الرسائل </span>
                        <br>
                        {{$contacts}}
                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-envelop3  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
         </div>

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-body bg-orange-400 has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <h3 class="no-margin">   </h3>
                        <span class="text-uppercase text-size-mini">عدد الأقسام </span>
                        <br>
                        {{$categories}}
                    </div>
                    <div class="media-right media-middle">
                        <i class="icon-bin2  icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>



    </div>


    <!-- 3D pie charts -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">المناطق</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="map" class="border-bottom" style="height: 500px;width: 100%"></div>
                </div>
            </div>
        </div>

    </div>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">تحليل حاﻻت صناديق الأعضاء</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="chart-container text-center content-group">
                        <div class="display-inline-block" id="google-pie-3d"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- /3D pie charts -->




@endsection


@section('script')
    {!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCsT140mx0UuES7ZwcfY28HuTUrTnDhxww&libraries=places&language=ar') !!}
    <script >
        var startUp = $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 24.7128688, lng: 46.8126935},
                zoom: 7
            });

            $.ajax({
                url: '{{url('/api/v1/cities')}}',
                type: 'GET',
                success: function (data) {
                    $.each(data.data, function (i, value) {
                        var Polyhon = new google.maps.Polygon({
                            paths: value.boards,
                            fillColor:value.hex_color,
                        });
                        Polyhon.setMap(map);
                    });
                }
            });


            // Construct the polygon.

         /*   var Rectangle = new google.maps.Rectangle({
                bounds: bounds,
                fillColor:color,
                editable: true,
                draggable: true,
            });
            $('#color').change(function () {
                Rectangle.setOptions({fillColor:$('#color').val()});
            });
            Rectangle.setMap(map);

            Rectangle.addListener('bounds_changed',function () {
                console.log(Rectangle.getBounds().toJSON());
                $('#bounders').val(JSON.stringify(Rectangle.getBounds().toJSON()));
            });
*/

        }
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script>
        // Initialize chart
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawPie3d);
        function drawPie3d() {
            // Data
            {{--var data = google.visualization.arrayToDataTable([--}}
                {{--['الحاله', 'عدد الطلبات'],--}}
                {{--['متاح',  {{\App\Models\Transportation::where('is_ended',0)->count()}}],--}}
                {{--['منتهي',  {{\App\Models\Transportation::where('is_ended',1)->count()}}],--}}
            {{--]);--}}

            // Options
            var options_pie_3d = {
                fontName: 'Cairo',
                is3D: true,
                height: 400,
                width: 904,
                chartArea: {
                    left: 30,
                    width: '95%',
                    height: '95%'
                }
            };


            // Instantiate and draw our chart, passing in some options.
            var pie_3d = new google.visualization.PieChart($('#google-pie-3d')[0]);
            pie_3d.draw(data, options_pie_3d);
        }

    </script>


@endsection
