
@if (count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif


<div class="form-group col-md-12 pull-left">
    <label>الاسم  </label>  <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
</div>


<div class="form-group col-md-6 pull-left">
    <label> الإيميل </label>   <span class="label bg-danger help-inline">مطلوب</span>
    {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'أكتب هنا  الايميل'])!!}
</div>

<div class="form-group col-md-6 pull-left">
    <label>باسورد </label>   <span class="label bg-danger help-inline">مطلوب</span>
    <input type="password" class="form-control" name="password" placeholder="اكتب هنا الباسورد">
</div>

<div class="form-group col-md-6 pull-left">
    <label>   إعادة كتابة الباسورد </label> <span class="label bg-danger help-inline">مطلوب</span>
    <input type="password" class="form-control" name="password_confirmation" placeholder="أعد هنا كتابة الباسورد">
</div>





<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>

@push('scripts')

@endpush